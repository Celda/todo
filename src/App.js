import React from 'react';
import logo from './logo.svg';
import './App.css';



class App extends React.Component{

  tacheInput = React.createRef();

  state={
    taches:[
      {id:1, nom:"Redux"},
      {id:2, nom:"Redaction memoire"},
      {id:3, nom: "Ecriture"}

    ],
    nouveauTache:'Figma'

    
  };
  handleDelete = id =>{
    const taches = this.state.taches.slice();

    const index = taches.findIndex(function(tache){
      return tache.id === id
    });

    taches.splice(index,1);
    
    this.setState({taches: taches});
  };
  handleSubmit  = (event) => {
    event.preventDefault();

    const id = new Date().getTime();

    const nom= this.state.nouveauTache;
    const taches= [...this.state.taches];
    taches.push({id,nom});

     this.setState({taches, nouveauTache: ""});

    
  }
  handleChange= (event)=> {
    const value = event.currentTarget.value;
    this.setState({nouveauTache:value});
  }
    
      
   
  render(){
    const title = "ToDo List";

    return(
    <div>
        <h1>{title}</h1>
        <ul>
          {this.state.taches.map(tache => (
       <li>
         {tache.nom} <button onClick={()=> this.handleDelete(tache.id)}>X</button>
        </li>
    ))}
          
        </ul>
        
        <form onSubmit={this.handleSubmit}>
          <input  value={this.state.nouveauTache} onChange={this.handleChange}type="text" placeholder="Ajouter une nouvelle tache"></input>
          <button>Confimer</button>
        </form>
      </div>
    
    )

  }
}
 
export default App;
